# Track Evaluation Code
# Code author: Paul Dreyer, dreyer@rand.org
# Last updated: 25 March 2019
#
# RAND Corporation researchers developed a multiphase challenge problem and metrics to assess
# data‐association algorithms. The problem uses a subset of Automatic Identification System data,
# used for maritime vessels, as an analog for data sets that consist of position, time, and velocity
# (speed and heading). This code assesses the data-association algorithms and produces metrics
# in order to compare the algorithms produced by the grant recipients.
#
# Section and page references in the comments below refer to the document:
# Chang, et. al, "Challenge-Problem Development and Results for Algorithms for Threat Detection"
# PR-4194-1-NGA, March 2019

import math
import csv
import datetime
import sys
import os

earth_rad_m = float(6367000)  # radius of earth in meters
knot_tenths_m_s_conv = 0.05144444  # converts tenths-of-knots to meters-per-second
invalid_sog = 1023  # from AIS data description
invalid_cog = 3600  # from AIS data description


class Itphv:
    """
    Itphv (ID, time, position, heading, velocity) is the structure used to define each node/record
    Explanation of variables:
    obj_id (string): The object ID associated to each AIS entry in the record data file
    pt_id (string): The MMSI identifier of the record (nodes in true tracks have identical MMSI values)
    t_sec (integer): timestamp of node in seconds (counted relative to 1/1/1970 12:00:00AM)
    lat_deg (double): latitude of node in degrees
    lon_deg (double): longitude of node in degrees
    h_deg (double): heading of node in degrees (note: AIS data is given in tenths of a degree)
    v_ms (double): velocity of node in meters per second (note: AIS data is given in tenths of a knot)
    true_trk_id (integer): numeric identifier of the true track to which the node belongs
    true_trk_idx (integer): numeric identifier of the (0-indexed) position of the node in its true track
    true_trk_id (integer): numeric identifier of the notional track to which the node belongs
    true_trk_idx (integer): numeric identifier of the (0-indexed) position of the node in its notional track
    """

    def __init__(self, obj_id, pt_id, t_sec, lat_deg, lon_deg, h_deg, v_ms, true_trk_id, true_trk_idx,
                 ntl_trk_id, ntl_trk_idx):
        """ Initialization function for an Itphv """
        self.obj_id = obj_id
        self.pt_id = pt_id
        self.t_sec = t_sec
        self.lat_deg = lat_deg
        self.lon_deg = lon_deg
        self.h_deg = h_deg
        self.v_ms = v_ms
        self.true_trk_idx = true_trk_idx
        self.true_trk_id = true_trk_id
        self.ntl_trk_idx = ntl_trk_idx
        self.ntl_trk_id = ntl_trk_id

    def __lt__(self, other):
        """ Less than function used for sorting"""
        return self.t_sec < other.t_sec

    def getobj_id(self):
        """ Returns the object's id """
        return self.obj_id

    def cog(self):
        """ Returns the course over ground in tenths of a degree """
        return int(self.h_deg * 10)

    def sog(self):
        """ Returns the speed over ground in tenths of a knot """
        return int(self.v_ms / knot_tenths_m_s_conv)

    def datetime_str(self):
        """ Returns the timestamp (SEQUENCE_DTTM) of the node """
        #  return (datetime.datetime(1970, 1, 1) + datetime.timedelta(seconds=self.t_sec)).strftime("%Y-%m-%d %H:%M:%S")
        return (datetime.datetime(1970, 1, 1) + datetime.timedelta(seconds=self.t_sec)).strftime("%H:%M:%S")

    def rdist_m(self, other):
        """ Returns the rhumb line (constant heading) distance between the object and another object (in m)
        Source: https://www.movable-type.co.uk/scripts/latlong.html """
        r = float(earth_rad_m)
        atan1 = math.atan(1.0)
        p1_lat_rad = self.lat_deg * (atan1 / 45.0)
        p1_lon_rad = self.lon_deg * (atan1 / 45.0)
        p2_lat_rad = other.lat_deg * (atan1 / 45.0)
        p2_lon_rad = other.lon_deg * (atan1 / 45.0)
        dphi = math.log(math.tan(p2_lat_rad / 2 + atan1) / math.tan(p1_lat_rad / 2 + atan1))
        dlat = p2_lat_rad - p1_lat_rad
        dlon = p2_lon_rad - p1_lon_rad

        if abs(dlon) > 4 * atan1:
            if dlon > 0:
                dlon = -1 * (8 * atan1 - dlon)
            else:
                dlon = 8 * atan1 + dlon

        if abs(dphi) <= 10 ** (-12):
            q = math.cos(p1_lat_rad)
        else:
            q = dlat / dphi

        return math.sqrt(dlat ** 2 + q ** 2 * dlon ** 2) * r


class ItphvList:
    """ ItphvList is a list of Itphv objects corresponding to tracks (both real and notional) """

    def __init__(self, first_pt):
        """ ItphvLists are initialized with a single Itphv record """
        self.first_pt = Itphv(first_pt.obj_id, str(first_pt.pt_id), first_pt.t_sec,
                              first_pt.lat_deg, first_pt.lon_deg,
                              first_pt.h_deg, first_pt.v_ms, first_pt.true_trk_id, first_pt.true_trk_idx,
                              first_pt.ntl_trk_id, first_pt.ntl_trk_idx)
        self.pt_list = [self.first_pt]

    def track_swap(self):
        """ This function counts track swaps (see p. 32) based off changes in true_trk_id """
        if len(self.pt_list) <= 1:
            return 0

        cnt = 0
        for i in range(1, len(self.pt_list)):
            if (self.pt_list[i - 1].true_trk_id != self.pt_list[i].true_trk_id) or \
                    (self.pt_list[i - 1].true_trk_idx + 1 != self.pt_list[i].true_trk_idx):
                cnt += 1
        return cnt

    def mapping_time(self):
        """ This function computes the total amount of time (in s) of edges on the track that match the track index
        of the first element of the track """
        if len(self.pt_list) <= 1:
            return 0

        cnt = 0
        for i in range(1, len(self.pt_list)):
            if (self.pt_list[0].true_trk_id == self.pt_list[i].true_trk_id) and \
                    (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                    (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                cnt += self.pt_list[i].t_sec - self.pt_list[i - 1].t_sec
        return cnt

    def mapping_dist(self):
        """ This function computes the total amount of distance (in m) of edges on the track that match the track index
        of the first element of the track """
        if len(self.pt_list) <= 1:
            return 0

        cnt = 0
        for i in range(1, len(self.pt_list)):
            if (self.pt_list[0].true_trk_id == self.pt_list[i].true_trk_id) and \
                    (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                    (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                cnt += self.pt_list[i].rdist_m(self.pt_list[i - 1])
        return cnt

    def continuity_time(self):
        """ This function computes the total amount of time (in s) of edges on the notional track that match
        edges on true tracks """

        if len(self.pt_list) <= 1:
            return 0

        cnt = 0
        for i in range(1, len(self.pt_list)):
            if (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                    (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                cnt += self.pt_list[i].t_sec - self.pt_list[i - 1].t_sec
        return cnt

    def continuity_dist(self):
        """ This function computes the total amount of distance (in m) of edges on the notional track that match
        edges on true tracks (see Section 5.2, p. 33) """

        if len(self.pt_list) <= 1:
            return 0

        cnt = 0
        for i in range(1, len(self.pt_list)):
            if (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                    (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                cnt += self.pt_list[i].rdist_m(self.pt_list[i - 1])
        return cnt

    def track_id(self):
        """ This function returns the true track id of the initial node of the list """
        return self.pt_list[0].true_trk_id

    def track_pt_id(self):
        """ This function returns the true point id of the initial node of the list """
        return self.pt_list[0].pt_id

    def track_id_last(self):
        """ This function returns the true track id of the final node of the list """
        return self.pt_list[len(self.pt_list) - 1].true_trk_id

    def track_time(self):
        """ This function returns the total track time (in s)"""
        return self.pt_list[len(self.pt_list) - 1].t_sec - self.pt_list[0].t_sec

    def track_dist(self):
        """ This function returns the total track (rhumb) distance (in m)"""

        if len(self.pt_list) <= 1:
            return 0
        cnt = 0.0
        for i in range(1, len(self.pt_list)):
            cnt += self.pt_list[i].rdist_m(self.pt_list[i - 1])
        return cnt

    def last_pt(self):
        """This function returns the final point of the track"""
        return self.pt_list[len(self.pt_list) - 1]


def track_eval(true_file, ntl_file, debug_flag):
    """
    This function does the track evaluation of the notional (ntl) file of tracks versus the true tracks
    It assumes input files (true_file, ntl_file) structured as described in Table 3.1-3.3
    It produces outputs as described in Chapter 5 of the document
    Explanation of variables:
    true_file (string): The name of the file containing the true tracks (possibly with the full path attached)
    ntl_file (string): The name of the file containing the notional tracks (from the data-association algorithm)

    True file format of header
    OBJECT_ID,VID,SEQUENCE_DTTM,LAT,LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND
    Notional file format of header
    OBJECT_ID,[VID],SEQUENCE_DTTM,LAT,LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,TRACK_ID (VID OPTIONAL)

    Note that VID can be replaced with MMSI and the code will still function
    """

    f_true = open(true_file, 'r')
    f_ntl = open(ntl_file, 'r')

    # Split string so ".csv" doesn't get transferred before name alterations
    output_file_summary = open(ntl_file.split('.')[0] + '_summary_eval.csv', 'w')

    output_file_summary.write('True Data Filename: {}\n'.format(true_file))
    output_file_summary.write('Notional Data Filename: {}\n'.format(ntl_file))

    all_pts_true = []
    params = list(csv.reader(f_true))
    invalid_file = False

    true_tracks = []
    num_true_tracks = 0
    ntl_tracks = []
    num_ntl_tracks = 0
    translator = {}

    id_track_num_dict = {}
    id_track_num_dict_ntl = {}
    obj_id_true_trk_id_dict = {}
    obj_id_true_trk_idx_dict = {}
    obj_id_ntl_trk_id_dict = {}
    obj_id_ntl_trk_idx_dict = {}

    if (params[0] !=
            ['OBJECT_ID', 'MMSI', 'SEQUENCE_DTTM', 'LAT', 'LON', 'SPEED_OVER_GROUND', 'COURSE_OVER_GROUND'] and
            params[0] !=
            ['OBJECT_ID', 'VID', 'SEQUENCE_DTTM', 'LAT', 'LON', 'SPEED_OVER_GROUND', 'COURSE_OVER_GROUND']):
        invalid_file = True
        print('Error: First line of true data file ' + true_file + ' does not have correct headers')
    else:
        # Reading the true points into all_pts_true

        for line in params[1:]:  # skip row zero as it contains the column headings

            t_obj_id = int(line[0])
            t_mmsi = str(line[1])
            t_lat = float(line[3])
            t_lon = float(line[4])
            t_time = datetime.datetime.strptime(line[2], "%H:%M:%S")
            t_sog = float(line[5])  # note, value is in knot-tenths, ignore 1023
            t_cog = float(line[6])  # note, value is in degree-tenths, ignore 3600

            if t_sog != invalid_sog and t_cog != invalid_cog:
                all_pts_true.append(
                    Itphv(t_obj_id, t_mmsi, (t_time - datetime.datetime(1970, 1, 1)).total_seconds(), t_lat,
                          t_lon, t_cog / 10, t_sog * knot_tenths_m_s_conv, 0, 0, 0, 0))
                # note conversions from degree-tenths to degrees, knot-tenths to m/s

        f_true.close()

        output_file_summary.write('True Points imported: {:d}\n'.format(len(all_pts_true)))
        all_pts_true.sort()

        # Assigning the true points in all_pts_true into their respective true tracks

        for pts in all_pts_true:
            if pts.pt_id in id_track_num_dict:
                t_track = id_track_num_dict[pts.pt_id]
                pts.true_trk_idx = len(true_tracks[t_track - 1].pt_list) + 1
                pts.true_trk_id = t_track
                true_tracks[t_track - 1].pt_list.append(pts)
            else:
                num_true_tracks += 1
                pts.true_trk_idx = 1
                pts.true_trk_id = num_true_tracks
                true_tracks.append(ItphvList(pts))
                id_track_num_dict[pts.pt_id] = num_true_tracks
            obj_id_true_trk_id_dict[pts.obj_id] = pts.true_trk_id
            obj_id_true_trk_idx_dict[pts.obj_id] = pts.true_trk_idx

    all_pts_ntl = []
    params = list(csv.reader(f_ntl))

    if (params[0] !=
            ['OBJECT_ID', 'MMSI', 'SEQUENCE_DTTM', 'LAT',
             'LON', 'SPEED_OVER_GROUND', 'COURSE_OVER_GROUND', 'TRACK_ID'] and
            params[0] !=
            ['OBJECT_ID', 'VID', 'SEQUENCE_DTTM', 'LAT',
             'LON', 'SPEED_OVER_GROUND', 'COURSE_OVER_GROUND', 'TRACK_ID'] and
            params[0] !=
            ['OBJECT_ID', 'SEQUENCE_DTTM', 'LAT',
             'LON', 'SPEED_OVER_GROUND', 'COURSE_OVER_GROUND', 'TRACK_ID']):
        invalid_file = True
        print('Error: First line of notional data file ' + ntl_file + ' does not have correct headers')
    else:

        # Flag to determine whether the file has VID values or not (affects element order)

        line = params[0]
        if line[1] == 'VID' or line[1] == 'MMSI':
            has_vid = 1
        else:
            has_vid = 0

        # Reading the notional points into all_pts_ntl

        for line in params[1:]:  # skip row zero as it contains the column headings

            t_obj_id = int(line[0])
            if has_vid == 1:
                t_mmsi = int(line[1])
            else:
                t_mmsi = 0
            t_time = datetime.datetime.strptime(line[1 + has_vid], "%H:%M:%S")
            t_lat = float(line[2 + has_vid])
            t_lon = float(line[3 + has_vid])
            t_sog = float(line[4 + has_vid])  # note, value is in knot-tenths, ignore 1023
            t_cog = float(line[5 + has_vid])  # note, value is in degree-tenths, ignore 3600
            t_track_id = int(line[6 + has_vid])

            if t_sog != invalid_sog and t_cog != invalid_cog:
                all_pts_ntl.append(
                    Itphv(t_obj_id, t_mmsi, (t_time - datetime.datetime(1970, 1, 1)).total_seconds(), t_lat,
                          t_lon, t_cog / 10, t_sog * knot_tenths_m_s_conv, 0, 0, t_track_id, 0))
                # note conversions from degree-tenths to degrees, knot-tenths to m/s
            f_ntl.close()

        output_file_summary.write('Notional Points imported: {:d}\n\n'.format(len(all_pts_ntl)))
        all_pts_ntl.sort()

        # Assigning the notional points in all_pts_ntl into their respective ntl tracks

        for pts in all_pts_ntl:

            # every point has a notional track id, this may reindex them

            if pts.ntl_trk_id in id_track_num_dict_ntl:
                t_track = id_track_num_dict_ntl[pts.ntl_trk_id]
                pts.ntl_trk_idx = len(ntl_tracks[translator[pts.ntl_trk_id] - 1].pt_list) + 1
                pts.ntl_trk_id = t_track
                pts.true_trk_id = obj_id_true_trk_id_dict[pts.obj_id]
                pts.true_trk_idx = obj_id_true_trk_idx_dict[pts.obj_id]
                ntl_tracks[translator[pts.ntl_trk_id] - 1].pt_list.append(pts)
            else:
                num_ntl_tracks += 1
                pts.ntl_trk_idx = 1
                pts.true_trk_id = obj_id_true_trk_id_dict[pts.obj_id]
                pts.true_trk_idx = obj_id_true_trk_idx_dict[pts.obj_id]
                ntl_tracks.append(ItphvList(pts))
                id_track_num_dict_ntl[pts.ntl_trk_id] = pts.ntl_trk_id
                translator[pts.ntl_trk_id] = len(ntl_tracks)
            obj_id_ntl_trk_id_dict[pts.obj_id] = pts.ntl_trk_id
            obj_id_ntl_trk_idx_dict[pts.obj_id] = pts.ntl_trk_idx

    if invalid_file:
        exit(-1)

    for true_track in true_tracks:
        for pt in true_track.pt_list:
            pt.ntl_trk_id = obj_id_ntl_trk_id_dict[pt.obj_id]
            pt.ntl_trk_idx = obj_id_ntl_trk_idx_dict[pt.obj_id]

    tot_swap = 0
    tot_mapping_time = 0
    tot_continuity_time = 0
    tot_track_time = 0
    tot_pts = 0

    tot_mapping_dist = 0.0
    tot_continuity_dist = 0.0
    tot_track_dist = 0.0

    # Debug - the following section of ~20 statements is for debugging purposes, if necessary
    if debug_flag:

        output_file_true = open(ntl_file.split('.')[0] + '_true_tracks_eval.csv', 'w')
        output_file_ntl = open(ntl_file.split('.')[0] + '_notional_tracks_eval.csv', 'w')

        output_file_true.write('OBJECT_ID,VID,SEQUENCE_DTTM,LAT,'
                               'LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,True Track Counter,Track Index,'
                               'Notional Track Counter,Track Index\n')
        output_file_ntl.write('OBJECT_ID,VID,SEQUENCE_DTTM,LAT,'
                              'LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,Notional Track Counter,Track Index,'
                              'True Track Counter,Track Index\n')

        for true_track in true_tracks:
            for pt in true_track.pt_list:
                output_file_true.write(str(pt.obj_id) + ',' + str(pt.pt_id) + ',' + pt.datetime_str() + ',' +
                                       str(pt.lat_deg) + ',' + str(pt.lon_deg) + ',' + str(pt.sog())
                                       + ',' + str(pt.cog()) + ',' + str(pt.true_trk_id) + ','
                                       + str(pt.true_trk_idx) + ',' + str(pt.ntl_trk_id) + ','
                                       + str(pt.ntl_trk_idx) + '\n')

        for ntl_track in ntl_tracks:
            for pt in ntl_track.pt_list:
                output_file_ntl.write(str(pt.obj_id) + ',' + str(pt.pt_id) + ',' + pt.datetime_str() + ',' +
                                      str(pt.lat_deg) + ',' + str(pt.lon_deg) + ',' + str(pt.sog())
                                      + ',' + str(pt.cog()) + ',' + str(pt.ntl_trk_id) + ','
                                      + str(pt.ntl_trk_idx) + ',' + str(pt.true_trk_id) + ','
                                      + str(pt.true_trk_idx) + '\n')

        output_file_true.close()
        output_file_ntl.close()

    # Note, we can continue to append new metrics to the right of the ones below.
    output_file_summary.write('True Track Counter,Track ID,Num Points,Track Time (s),Track Dist (m),'
                              + 'Completeness Score,Closest Notional Track\n')

    true_ctr = 0

    num_missed_tracks = 0  # start nodes in true tracks, not notional ones
    num_extra_tracks = 0  # start nodes in notional tracks, not true ones
    num_merged_tracks = 0  # end nodes in true tracks, not notional ones
    num_broken_tracks = 0  # end nodes in notional tracks, not true ones

    tot_true_track_time = 0
    tot_true_track_dist = 0

    # Completeness score calculation (See Section 5.3, p. 33-34)

    for true_track in true_tracks:
        true_ctr += 1
        completeness_score = 0
        best_track_id = 0

        ntl_ctr = 0
        for ntl_track in ntl_tracks:
            pt_ctr = 0
            ntl_ctr += 1
            for ntl_pt in ntl_track.pt_list:
                for true_pt in true_track.pt_list:
                    if ntl_pt.obj_id == true_pt.obj_id:
                        pt_ctr += 1
            if pt_ctr > completeness_score:
                completeness_score = pt_ctr
                best_track_id = ntl_ctr

        output_file_summary.write('{},{},{:d},{:f},{:f},{:f},{:d}\n'.format(true_ctr, true_track.first_pt.pt_id,
                                                                            len(true_track.pt_list),
                                                                            true_track.track_time(),
                                                                            true_track.track_dist(),
                                                                            completeness_score / len(
                                                                                true_track.pt_list), best_track_id))

        # Missed/merged tracks calculation (see Section 5.1, p. 30-32)

        missed_flag = True
        merged_flag = True
        for ntl_track in ntl_tracks:
            if true_track.first_pt.obj_id == ntl_track.first_pt.obj_id:
                missed_flag = False
            if true_track.last_pt().obj_id == ntl_track.last_pt().obj_id:
                merged_flag = False
        if missed_flag:
            num_missed_tracks += 1
        if merged_flag:
            num_merged_tracks += 1

        # calculate true track distance
        tot_true_track_time += true_track.track_time()
        tot_true_track_dist += true_track.track_dist()

    # Note, we can continue to append new metrics to the right of the ones below.

    output_file_summary.write('\nNtl Track Counter,Num Points,Track Time (s),Track Dist (m),Track Swaps\n')

    ntl_ctr = 0

    for ntl_track in ntl_tracks:
        ntl_ctr += 1

        # Extra/broken tracks calculation (see Section 5.1, p. 30-32)

        extra_flag = True
        broken_flag = True
        for true_track in true_tracks:
            if true_track.first_pt.obj_id == ntl_track.first_pt.obj_id:
                extra_flag = False
            if true_track.last_pt().obj_id == ntl_track.last_pt().obj_id:
                broken_flag = False
        if extra_flag:
            num_extra_tracks += 1
        if broken_flag:
            num_broken_tracks += 1

        # Track swaps, mapping and continuity calculations for each notional track (Section 5.2, p. 33)

        t_swap = ntl_track.track_swap()
        tot_swap += t_swap

        t_mapping_time = ntl_track.mapping_time()
        t_mapping_dist = ntl_track.mapping_dist()
        tot_mapping_time += t_mapping_time
        tot_mapping_dist += t_mapping_dist

        t_pts = len(ntl_track.pt_list)
        tot_pts += t_pts

        t_continuity_time = ntl_track.continuity_time()
        t_continuity_dist = ntl_track.continuity_dist()
        tot_continuity_time += t_continuity_time
        tot_continuity_dist += t_continuity_dist

        t_track_time = ntl_track.track_time()
        t_track_dist = ntl_track.track_dist()
        tot_track_time += t_track_time
        tot_track_dist += t_track_dist

        output_file_summary.write('{},{:d},{:f},{:f},{:d}\n'.format(ntl_ctr, t_pts, t_track_time,
                                                                    t_track_dist, t_swap))

    output_file_summary.write('Totals,{:d},{:f},{:f},{:d}\n'.format(tot_pts, tot_track_time,
                                                                    tot_track_dist,
                                                                    tot_swap))

    output_file_summary.write('\n')
    if tot_track_dist > 0:
        output_file_summary.write('Continuity Dist Score: {:.3f}\n'.format(tot_continuity_dist / tot_true_track_dist))
    output_file_summary.write('{} True Tracks/{} Notional Tracks\n'.format(num_true_tracks, num_ntl_tracks))
    output_file_summary.write('{} Missed Tracks/{} Extra Tracks/{} Merged Tracks/{} Broken Tracks\n'
                              .format(num_missed_tracks, num_extra_tracks, num_merged_tracks, num_broken_tracks))

    output_file_summary.close()


def main():
    if len(sys.argv) != 3:
        print('usage: python TrackEval.py <true_data_file> <notional_data_file> (both in quotes)')
        exit(1)

    debug_flag = False  # change to output additional data files
    t_true_file = sys.argv[1]
    t_ntl_file = sys.argv[2]
    if not os.path.exists(t_true_file):
        print('Error: True Data File {} Does Not Exist.'.format(t_true_file))
    if not os.path.exists(t_ntl_file):
        print('Error: Notional Data File {} Does Not Exist.'.format(t_ntl_file))
    if not os.path.exists(t_ntl_file) or not os.path.exists(t_true_file):
        exit(1)

    track_eval(t_true_file, t_ntl_file, debug_flag)


main()
