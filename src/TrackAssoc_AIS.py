import hashlib
import math
import csv
import datetime
from graphics import *


class Itphv:

    def __init__(self, obj_id, pt_id, t_sec, lat_deg, lon_deg, h_deg, v_ms, true_trk_id, true_trk_idx,
                 ntl_trk_id, ntl_trk_idx):
        self.obj_id = obj_id
        self.pt_id = pt_id
        self.t_sec = t_sec
        self.lat_deg = lat_deg
        self.lon_deg = lon_deg
        self.h_deg = h_deg
        self.v_ms = v_ms
        self.true_trk_idx = true_trk_idx
        self.true_trk_id = true_trk_id
        self.ntl_trk_idx = ntl_trk_idx
        self.ntl_trk_id = ntl_trk_id

    def __lt__(self, other):
        return self.t_sec < other.t_sec

    def getobj_id(self):
        return self.obj_id

    def cog(self):
        return int(self.h_deg * 10)

    def sog(self):
        return int(self.v_ms / 0.05144444)

    def datetime_str(self):
        return (datetime.datetime(1970, 1, 1) + datetime.timedelta(seconds=self.t_sec)).strftime("%H:%M:%S")

    def rdist_m(self, other):
        r = float(6367000)
        atan1 = math.atan(1.0)
        p1_lat_rad = self.lat_deg * (atan1 / 45.0)
        p1_lon_rad = self.lon_deg * (atan1 / 45.0)
        p2_lat_rad = other.lat_deg * (atan1 / 45.0)
        p2_lon_rad = other.lon_deg * (atan1 / 45.0)
        dphi = math.log(math.tan(p2_lat_rad / 2 + atan1) / math.tan(p1_lat_rad / 2 + atan1))
        dlat = p2_lat_rad - p1_lat_rad
        dlon = p2_lon_rad - p1_lon_rad

        if abs(dlon) > 4 * atan1:
            if dlon > 0:
                dlon = -1 * (8 * atan1 - dlon)
            else:
                dlon = 8 * atan1 + dlon

        if abs(dphi) <= 10 ** (-12):
            q = math.cos(p1_lat_rad)
        else:
            q = dlat / dphi

        return math.sqrt(dlat ** 2 + q ** 2 * dlon ** 2) * r


class ItphvList:

    def __init__(self, first_pt):
        self.first_pt = Itphv(first_pt.obj_id, str(first_pt.pt_id), first_pt.t_sec,
                              first_pt.lat_deg, first_pt.lon_deg,
                              first_pt.h_deg, first_pt.v_ms, first_pt.true_trk_id, first_pt.true_trk_idx,
                              first_pt.ntl_trk_id, first_pt.ntl_trk_idx)
        self.pt_list = [self.first_pt]

    def track_swap(self):
        if len(self.pt_list) <= 1:
            return 0
        else:
            cnt = 0
            for i in range(1, len(self.pt_list)):
                if (self.pt_list[i - 1].true_trk_id != self.pt_list[i].true_trk_id) or \
                        (self.pt_list[i - 1].true_trk_idx + 1 != self.pt_list[i].true_trk_idx):
                    cnt += 1
            return cnt

    def mapping_time(self):
        if len(self.pt_list) <= 1:
            return 0
        else:
            cnt = 0
            for i in range(1, len(self.pt_list)):
                if (self.pt_list[0].true_trk_id == self.pt_list[i].true_trk_id) and \
                        (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                        (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                    cnt += self.pt_list[i].t_sec - self.pt_list[i - 1].t_sec
            return cnt

    def mapping_dist(self):
        if len(self.pt_list) <= 1:
            return 0
        else:
            cnt = 0
            for i in range(1, len(self.pt_list)):
                if (self.pt_list[0].true_trk_id == self.pt_list[i].true_trk_id) and \
                        (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                        (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                    cnt += self.pt_list[i].rdist_m(self.pt_list[i - 1])
            return cnt

    def continuity_time(self):
        if len(self.pt_list) <= 1:
            return 0
        else:
            cnt = 0
            for i in range(1, len(self.pt_list)):
                if (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                        (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                    cnt += self.pt_list[i].t_sec - self.pt_list[i - 1].t_sec
            return cnt

    def continuity_dist(self):
        if len(self.pt_list) <= 1:
            return 0
        else:
            cnt = 0
            for i in range(1, len(self.pt_list)):
                if (self.pt_list[i - 1].true_trk_idx + 1 == self.pt_list[i].true_trk_idx) and \
                        (self.pt_list[i - 1].true_trk_id == self.pt_list[i].true_trk_id):
                    cnt += self.pt_list[i].rdist_m(self.pt_list[i - 1])
            return cnt

    def track_id(self):
        return self.pt_list[0].true_trk_id

    def track_pt_id(self):
        return self.pt_list[0].pt_id

    def track_id_last(self):
        return self.pt_list[len(self.pt_list) - 1].true_trk_id

    def track_time(self):
        return self.pt_list[len(self.pt_list) - 1].t_sec - self.pt_list[0].t_sec

    def track_dist(self):
        if len(self.pt_list) <= 1:
            return 0
        else:
            cnt = 0.0
            for i in range(1, len(self.pt_list)):
                cnt += self.pt_list[i].rdist_m(self.pt_list[i - 1])
            return cnt

    def last_pt(self):
        return self.pt_list[len(self.pt_list) - 1]


def is_between(val, x_min, x_max):
    return (val >= x_min) and (val <= x_max)


def m_str(t_1, t_2, v_1, v_2, dist):
    # m_str is the slope of the segment between (t_1,v_1) and (t_2,v_2) to the point (t_*, v_*) such that
    # the segment between (t_1,v_1) (t_*,v_*) has slope m, (t_*, v_*) (t_2, v_2) has slope -m, and area of curve
    # under line is dist

    # m satisfies D = v_1*(t* -t_1) + m/2*(t* - t_1)^2 + v_2*(t_2 - t*) + m/2*(t_2 - t*)^2
    # t* = t_2 - [(v_1-v_2) + m*(t_2-t_1)]/(2m)

    # Two solutions to quadratic

    # m1 = 1 / (t_1 - t_2)^2 * (2D + t_1*v_1 - t_2*v_2 + t_1*v_2 - t_2*v_2 -/+
    # math.sqrt(2)*math.sqrt(2D^2 + 2D*(t_1-t_2)(v_1 + v_2) + (t_1-t_2)^2(v_1^2 + v_2^2)

    if dist == 0 and t_1 == t_2:
        return 0
    elif t_1 == t_2:
        return -10 ** 9
    elif v_1 == v_2:
        return (dist - (t_2 - t_1) * v_1) / ((t_2 - t_1) / 2) ** 2
    elif (2 * dist ** 2 + 2 * dist * (t_1 - t_2) * (v_1 + v_2) + (t_1 - t_2) ** 2 * (v_1 ** 2 + v_2 ** 2)) < - 0.001:
        return -10 ** 9
    else:
        if (2 * dist ** 2 + 2 * dist * (t_1 - t_2) * (v_1 + v_2) + (t_1 - t_2) ** 2 * (v_1 ** 2 + v_2 ** 2)) < 0:
            m_star_1 = 1 / (t_1 - t_2) ** 2 * (2 * dist + t_1 * v_1 - t_2 * v_1 + t_1 * v_2 - t_2 * v_2)
            m_star_2 = 1 / (t_1 - t_2) ** 2 * (2 * dist + t_1 * v_1 - t_2 * v_1 + t_1 * v_2 - t_2 * v_2)
        else:
            m_star_1 = 1 / (t_1 - t_2) ** 2 * (2 * dist + t_1 * v_1 - t_2 * v_1 + t_1 * v_2 - t_2 * v_2 +
                                               math.sqrt(2) * math.sqrt(
                        2 * dist ** 2 + 2 * dist * (t_1 - t_2) * (v_1 + v_2) +
                        (t_1 - t_2) ** 2 * (v_1 ** 2 + v_2 ** 2)))
            m_star_2 = 1 / (t_1 - t_2) ** 2 * (2 * dist + t_1 * v_1 - t_2 * v_1 + t_1 * v_2 - t_2 * v_2 -
                                               math.sqrt(2) * math.sqrt(
                        2 * dist ** 2 + 2 * dist * (t_1 - t_2) * (v_1 + v_2) +
                        (t_1 - t_2) ** 2 * (v_1 ** 2 + v_2 ** 2)))

        if m_star_1 == (v_2 - v_1) / (t_2 - t_1):
            return m_star_1
        elif m_star_1 == -1 * (v_2 - v_1) / (t_2 - t_1):
            t_star_1 = t_1
            v_err_1 = 0
        else:
            t_star_1 = t_2 - (v_1 - v_2 + m_star_1 * (t_2 - t_1)) / (2 * m_star_1)
            v_err_1 = abs(m_star_1 - (v_2 - v_1) / (t_2 - t_1)) * (t_star_1 - t_1)

        if m_star_2 == (v_2 - v_1) / (t_2 - t_1):
            return m_star_2
        elif m_star_2 == -1 * (v_2 - v_1) / (t_2 - t_1):
            t_star_2 = t_1
            v_err_2 = 0
        else:
            t_star_2 = t_2 - (v_1 - v_2 + m_star_2 * (t_2 - t_1)) / (2 * m_star_2)
            v_err_2 = abs(m_star_2 - (v_2 - v_1) / (t_2 - t_1)) * (t_star_2 - t_1)

        if is_between(t_star_1, t_1, t_2) and is_between(t_star_2, t_1, t_2):
            if v_err_1 < v_err_2:
                return m_star_1
            else:
                return m_star_2
        elif is_between(t_star_1, t_1, t_2):
            return m_star_1
        elif is_between(t_star_2, t_1, t_2):
            return m_star_2
        else:
            return -10 ** 9


def t_str(t_1, t_2, v_1, v_2, dist):
    m_star = m_str(t_1, t_2, v_1, v_2, dist)

    if m_star == -10 ** 9:
        return -10 ** 9
    elif m_star == 0:
        return t_2
    else:
        return t_2 - ((v_1 - v_2) + m_star * (t_2 - t_1)) / (2 * m_star)


def v_str(t_1, t_2, v_1, v_2, dist):
    m_star = m_str(t_1, t_2, v_1, v_2, dist)
    t_star = t_str(t_1, t_2, v_1, v_2, dist)
    if t_star == -10 ** 9:
        return -10 ** 9
    else:
        return v_1 + m_star * (t_star - t_1)


def v_str_err(t_1, t_2, v_1, v_2, dist):
    m_star = m_str(t_1, t_2, v_1, v_2, dist)
    t_star = t_str(t_1, t_2, v_1, v_2, dist)
    if t_1 == t_2:
        return 0
    if t_star == -10 ** 9:
        return -10 ** 9
    if t_star == t_1:
        return -10 ** 9
    else:
        return abs((m_star - (v_2 - v_1) / (t_2 - t_1)) * (t_star - t_1))


def interp(x, x_min, x_max, map_min, map_max):
    if x_max == x_min:
        return map_min
    elif x <= x_min:
        return map_min
    elif x >= x_max:
        return map_max
    else:
        return map_min + (map_max - map_min) * (x - x_min) / (x_max - x_min)


def track_assoc_AIS(data_file, alpha_v, alpha_dh, alpha_d2h, max_v_ms):
    # OLD assumption re: data file
    # file format is: header line, then comma separated
    # Maritime Mobile Service Identify (MMSI) no.
    # Time(t in sec)
    # Location(lon / lat in decimal degrees)
    # Heading(h in degrees)
    # Speed(v in m/s)

    # New assumption re: data file (AIS Format from Chuck)

    # file format is: header line, then comma separated
    # Index [OBJECT_ID]
    # Maritime Mobile Service Identify (MMSI) no. [VID NOTE THIS IS NOT NECESSARILY PRESENT]
    # Sequence time (hh:mm:ss)
    # Latitude (decimal degrees)
    # Longitude (decimal degrees)
    # Speed over ground (in knot-tenths (1 knot-tenth = 0.0514444 m/s), 1023 = IGNORE)
    # Course over ground (in degree-tenths, 3600 = IGNORE)
    # Heading (not used)
    # sequence date (not used)
    # sequence time (not used)

    f = open(data_file, 'rU')

    x_min = 100000
    y_min = 100000
    x_max = -100000
    y_max = -100000

    all_pts = []
    params = list(csv.reader(f))

    line = params[0]
    if line[1] == 'VID':
        has_vid = 1
    else:
        has_vid = 0

    for line in params[1:]:  # skip row zero as it contains the column headings

        t_obj_id = int(line[0])
        if has_vid == 1:
            t_mmsi = int(line[1])
        else:
            t_mmsi = 0
        t_time = datetime.datetime.strptime(line[1 + has_vid], "%H:%M:%S")
        t_lat = float(line[2 + has_vid])
        t_lon = float(line[3 + has_vid])
        #  t_time = datetime.datetime.strptime(line[4], "%Y-%m-%d %H:%M:%S.%f")

        t_sog = float(line[4 + has_vid])  # note, value is in knot-tenths, ignore 1023
        t_cog = float(line[5 + has_vid])  # note, value is in degree-tenths, ignore 3600

        #  if min_time <= t_time <= max_time and t_sog != 1023 and t_cog != 3600 and t_sog > 10:
        if t_sog != 1023 and t_cog != 3600:
            all_pts.append(Itphv(t_obj_id, t_mmsi, (t_time - datetime.datetime(1970, 1, 1)).total_seconds(), t_lat,
                                 t_lon, t_cog / 10, t_sog * 0.05144444, 0, 0, 0, 0))  # note conv to degrees, m/s
            x_min = min(x_min, t_lon)
            y_min = min(y_min, t_lat)
            x_max = max(x_max, t_lon)
            y_max = max(y_max, t_lat)

    f.close()
    
    data_file_name, data_file_extension = os.path.splitext(data_file)
    output_file_notional = open(data_file_name + '_notional'+data_file_extension, 'w')

    all_pts.sort()

    if has_vid == 1:
        output_file_summary = open(data_file_name + '_summary', 'w')
        output_file_true = open(data_file_name + '_true', 'w')
        output_file_true_tracks = open(data_file_name + '_true_tracks', 'w')
        output_file_notional_tracks = open(data_file_name + '_notional_tracks'+data_file_extension, 'w')
        output_file_summary.write('Data Filename: {}\n'.format(data_file))
        output_file_summary.write('alpha_v: {}\n'.format(alpha_v))
        output_file_summary.write('alpha_dh: {}\n'.format(alpha_dh))
        output_file_summary.write('alpha_d2h: {}\n'.format(alpha_d2h))
        output_file_summary.write('max_v_ms: {}\n'.format(max_v_ms))
        output_file_summary.write('Points imported: {:d}\n\n'.format(len(all_pts)))

        true_tracks = []
        num_true_tracks = 0
        id_track_num_dict = {}

        for pts in all_pts:
            if pts.pt_id in id_track_num_dict:
                t_track = id_track_num_dict[pts.pt_id]
                pts.true_trk_idx = len(true_tracks[t_track - 1].pt_list) + 1
                pts.true_trk_id = t_track
                true_tracks[t_track - 1].pt_list.append(pts)
            else:
                num_true_tracks += 1
                pts.true_trk_idx = 1
                pts.true_trk_id = num_true_tracks
                true_tracks.append(ItphvList(pts))
                id_track_num_dict[pts.pt_id] = num_true_tracks

        #   output_file_summary.write('{:d} True Tracks\n\n'.format(num_true_tracks))

    num_ntl_tracks = 0
    ntl_tracks = []

    for pt in all_pts:
        if num_ntl_tracks == 0:
            num_ntl_tracks += 1
            pt.ntl_trk_id = 1
            pt.ntl_trk_idx = 1
            ntl_tracks.append(ItphvList(pt))
        else:
            best_score = 10 ** 9
            best_track = ItphvList(pt)

            for ntl_track in ntl_tracks:
                p_1 = ntl_track.last_pt()

                if pt.t_sec == p_1.t_sec:  # dt = 0, don't calculate t_dh either
                    t_dh = 0
                else:
                    t_dh = abs(pt.h_deg - p_1.h_deg)
                    t_dh = min(t_dh, 360 - t_dh)
                    t_dh = t_dh / (pt.t_sec - p_1.t_sec)

                if len(ntl_track.pt_list) == 1:  # no calculation of d2h
                    t_d2h = 0
                else:
                    p_0 = ntl_track.pt_list[len(ntl_track.pt_list) - 2]
                    if p_1.t_sec == p_0.t_sec:
                        t_d2h = 0
                    else:
                        t_dh2 = abs(p_1.h_deg - p_0.h_deg)
                        t_dh2 = min(t_dh2, 360 - t_dh2)
                        t_dh2 = t_dh2 / (p_1.t_sec - p_0.t_sec)
                        t_d2h = abs((t_dh2 - t_dh) / (p_1.t_sec - p_0.t_sec))

                t_1 = p_1.t_sec
                t_2 = pt.t_sec

                v_1 = p_1.v_ms
                v_2 = pt.v_ms

                dist = pt.rdist_m(p_1)

                v_star = v_str(t_1, t_2, v_1, v_2, dist)
                # m_star = m_str(t_1, t_2, v_1, v_2, dist)
                # t_star = t_str(t_1, t_2, v_1, v_2, dist)

                # if v_star != -10 ** 9 and v_star >= 0 and (v_star <= max_v_ms):
                if v_star != -10 ** 9 and (abs(v_star) <= max_v_ms):

                    v_star_err = v_str_err(t_1, t_2, v_1, v_2, dist)

                    # print('{}\n'.format(v_star_err))

                    t_score = alpha_v * v_star_err + alpha_dh * t_dh + alpha_d2h * t_d2h

                    if t_score < best_score:
                        best_score = t_score
                        best_track = ntl_track

            if best_score == 10 ** 9:
                num_ntl_tracks += 1
                pt.ntl_trk_id = num_ntl_tracks
                pt.ntl_trk_idx = 1
                best_track.pt_list[0].ntl_trk_id = num_ntl_tracks
                best_track.pt_list[0].ntl_trk_idx = 1
                ntl_tracks.append(best_track)
            else:
                for ntl_track in ntl_tracks:
                    if ntl_track == best_track:
                        pt.ntl_trk_id = best_track.pt_list[0].ntl_trk_id
                        pt.ntl_trk_idx = len(best_track.pt_list) + 1
                        ntl_track.pt_list.append(pt)

    all_pts_ntl = []

    for ntl_track in ntl_tracks:
        for pt in ntl_track.pt_list:
            all_pts_ntl.append(pt)

    all_pts_ntl.sort(key=lambda Itphv: Itphv.obj_id)

    if has_vid == 1:
        output_file_notional.write('OBJECT_ID,VID,SEQUENCE_DTTM,LAT,'
                                   'LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,TRACK_ID\n')
    else:
        output_file_notional.write('OBJECT_ID,SEQUENCE_DTTM,LAT,'
                                   'LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,TRACK_ID\n')

    for pt in all_pts_ntl:

        if has_vid == 1:
            output_file_notional.write(str(pt.obj_id) + ',' + str(pt.pt_id) + ',' + pt.datetime_str() + ',' +
                                       str(pt.lat_deg) + ',' + str(pt.lon_deg) + ',' + str(pt.sog())
                                       + ',' + str(pt.cog()) + ',' + str(pt.ntl_trk_id) + '\n')
        else:
            output_file_notional.write(str(pt.obj_id) + ',' + pt.datetime_str() + ',' +
                                       str(pt.lat_deg) + ',' + str(pt.lon_deg) + ',' + str(pt.sog())
                                       + ',' + str(pt.cog()) + ',' + str(pt.ntl_trk_id) + '\n')

    output_file_notional.close()

    if has_vid == 1:

        tot_swap = 0
        tot_mapping_time = 0
        tot_continuity_time = 0
        tot_track_time = 0
        tot_pts = 0

        tot_mapping_dist = 0.0
        tot_continuity_dist = 0.0
        tot_track_dist = 0.0

        #    output_file_summary.write('{} Notional Tracks\n\n'.format(num_ntl_tracks))

        true_ctr = 0

        all_pts_true = []

        output_file_true.write('OBJECT_ID,VID,SEQUENCE_DTTM,LAT,'
                               'LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,True Track Counter,Point Track Index\n')

        output_file_true_tracks.write('OBJECT_ID,VID,SEQUENCE_DTTM,LAT,'
                                      'LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,'
                                      'True Track Counter,Track Index,Notional Track Counter,Track Index\n')

        for true_track in true_tracks:
            for pt in true_track.pt_list:
                all_pts_true.append(pt)
                output_file_true_tracks.write(str(pt.obj_id) + ',' + str(pt.pt_id) + ',' + pt.datetime_str() + ',' +
                                              str(pt.lat_deg) + ',' + str(pt.lon_deg) + ',' + str(pt.sog())
                                              + ',' + str(pt.cog()) + ',' + str(pt.true_trk_id) + ','
                                              + str(pt.true_trk_idx) + ',' + str(pt.ntl_trk_id) + ','
                                              + str(pt.ntl_trk_idx) + '\n')

        all_pts_true.sort(key=lambda Itphv: Itphv.obj_id)

        for pt in all_pts_true:
            output_file_true.write(str(pt.obj_id) + ',' + str(pt.pt_id) + ',' + pt.datetime_str() + ',' +
                                   str(pt.lat_deg) + ',' + str(pt.lon_deg) + ',' + str(pt.sog())
                                   + ',' + str(pt.cog()) + ',' + str(pt.true_trk_id) + ','
                                   + str(pt.true_trk_idx) + '\n')

        # Note, we can continue to append new metrics to the right of the ones below.
        output_file_summary.write('True Track Counter,Track ID,Num Points,Track Time (s),Track Dist (m),'
                                  + 'Completeness Score,Closest Notional Track\n')

        true_ctr = 0

        num_missed_tracks = 0  # start nodes in true tracks, not notional ones
        num_extra_tracks = 0  # start nodes in notional tracks, not true ones
        num_merged_tracks = 0  # end nodes in true tracks, not notional ones
        num_broken_tracks = 0  # end nodes in notional tracks, not true ones

        for true_track in true_tracks:
            true_ctr += 1

            completeness_score = 0
            best_track_id = 0

            ntl_ctr = 0
            for ntl_track in ntl_tracks:
                pt_ctr = 0
                ntl_ctr += 1
                for pt in ntl_track.pt_list:
                    if pt.pt_id == true_track.first_pt.pt_id:
                        pt_ctr += 1
                if pt_ctr > completeness_score:
                    completeness_score = pt_ctr
                    best_track_id = ntl_ctr

            output_file_summary.write('{},{},{:d},{:f},{:f},{:f},{:d}\n'.format(true_ctr, true_track.first_pt.pt_id,
                                                                                len(true_track.pt_list),
                                                                                true_track.track_time(),
                                                                                true_track.track_dist(),
                                                                                completeness_score / len(
                                                                                    true_track.pt_list), best_track_id))
            missed_flag = True
            merged_flag = True
            for ntl_track in ntl_tracks:
                if true_track.first_pt.obj_id == ntl_track.first_pt.obj_id:
                    missed_flag = False
                if true_track.last_pt().obj_id == ntl_track.last_pt().obj_id:
                    merged_flag = False
            if missed_flag:
                num_missed_tracks += 1
            if merged_flag:
                num_merged_tracks += 1

        # Note, we can continue to append new metrics to the right of the ones below.

        output_file_summary.write('\nNtl Track Counter,Num Points,Track Time (s),Track Dist (m),Track Swaps,' +
                                  'Mapping Time (s),Continuity Time (s),' +
                                  'Mapping Dist (m),Continuity Dist (m)\n')

        output_file_notional_tracks.write('OBJECT_ID,VID,SEQUENCE_DTTM,LAT,'
                                          'LON,SPEED_OVER_GROUND,COURSE_OVER_GROUND,'
                                          'Notional Track Counter,Track Index,True Track Counter,Track Index\n')
        for ntl_track in ntl_tracks:
            for pt in ntl_track.pt_list:
                output_file_notional_tracks.write(str(pt.obj_id) + ',' + str(pt.pt_id) + ',' + pt.datetime_str() + ','
                                                  + str(pt.lat_deg) + ',' + str(pt.lon_deg) + ',' + str(pt.sog())
                                                  + ',' + str(pt.cog()) + ',' + str(pt.ntl_trk_id) + ','
                                                  + str(pt.ntl_trk_idx) + ',' + str(pt.true_trk_id) + ','
                                                  + str(pt.true_trk_idx) + '\n')

        ntl_ctr = 0

        for ntl_track in ntl_tracks:
            ntl_ctr += 1
            extra_flag = True
            broken_flag = True
            for true_track in true_tracks:
                if true_track.first_pt.obj_id == ntl_track.first_pt.obj_id:
                    extra_flag = False
                if true_track.last_pt().obj_id == ntl_track.last_pt().obj_id:
                    broken_flag = False
            if extra_flag:
                num_extra_tracks += 1
            if broken_flag:
                num_broken_tracks += 1

            t_swap = ntl_track.track_swap()
            # output_file.write('\n{} Track Swap(s)\n'.format(t_swap))
            tot_swap += t_swap

            t_mapping_time = ntl_track.mapping_time()
            t_mapping_dist = ntl_track.mapping_dist()
            # output_file.write('{} second(s) of correct mapping\n'.format(t_mapping_time))
            tot_mapping_time += t_mapping_time
            tot_mapping_dist += t_mapping_dist

            t_pts = len(ntl_track.pt_list)
            tot_pts += t_pts

            t_continuity_time = ntl_track.continuity_time()
            t_continuity_dist = ntl_track.continuity_dist()
            # output_file.write('{} second(s) of correct continuity\n'.format(t_continuity_time))
            tot_continuity_time += t_continuity_time
            tot_continuity_dist += t_continuity_dist

            t_track_time = ntl_track.track_time()
            t_track_dist = ntl_track.track_dist()
            # output_file.write('{} second(s) of track length\n\n'.format(t_track_length))
            tot_track_time += t_track_time
            tot_track_dist += t_track_dist

            output_file_summary.write('{},{:d},{:f},{:f},{:f},{:f},{:f},{:f}\n'.format(ntl_ctr, t_pts,
                                                                                       t_track_time, t_track_dist,
                                                                                       t_swap,
                                                                                       t_mapping_time,
                                                                                       t_continuity_time,
                                                                                       t_mapping_dist,
                                                                                       t_continuity_dist))

        output_file_summary.write('Totals,{:d},{:f},{:f},{:f},{:f},{:f},{:f},{:f}\n'.format(tot_pts, tot_track_time,
                                                                                            tot_track_dist,
                                                                                            tot_swap,
                                                                                            tot_mapping_time,
                                                                                            tot_continuity_time,
                                                                                            tot_mapping_dist,
                                                                                            tot_continuity_dist))

        #    output_file.write('{} total second(s) of correct mapping\n'.format(tot_mapping_time))
        #    output_file.write('{} total second(s) of correct continuity\n'.format(tot_continuity_time))
        #    output_file.write('{} total second(s) of notional track length\n\n'.format(tot_track_length))
        #    output_file.write('{} total track swap(s)\n'.format(tot_swap))
        output_file_summary.write('\n')
        if tot_track_time > 0:
            output_file_summary.write('Mapping Time Score: {:.3f}\n'.format(tot_mapping_time / tot_track_time))
            output_file_summary.write('Continuity Time Score: {:.3f}\n'.format(tot_continuity_time / tot_track_time))
        if tot_track_dist > 0:
            output_file_summary.write('Mapping Dist Score: {:.3f}\n'.format(tot_mapping_dist / tot_track_dist))
            output_file_summary.write('Continuity Dist Score: {:.3f}\n'.format(tot_continuity_dist / tot_track_dist))
        output_file_summary.write('{} True Tracks/{} Notional Tracks\n'.format(num_true_tracks, num_ntl_tracks))
        output_file_summary.write('{} Missed Tracks/{} Extra Tracks/{} Merged Tracks/{} Broken Tracks\n'
                                  .format(num_missed_tracks, num_extra_tracks, num_merged_tracks, num_broken_tracks))

        output_file_true.close()
        output_file_summary.close()
        output_file_true_tracks.close()
        output_file_notional_tracks.close()

        # graphics code goes here

        colorlist = ['aliceblue', 'antiquewhite', 'aqua', 'aquamarine', 'azure', 'beige', 'bisque', 'black',
                     'blanchedalmond', 'blue', 'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse',
                     'chocolate',
                     'coral', 'cornflowerblue', 'cornsilk', 'crimson', 'cyan', 'darkblue', 'darkcyan', 'darkgoldenrod',
                     'darkgray', 'darkgreen', 'darkkhaki', 'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid',
                     'darkred', 'darksalmon', 'darkseagreen', 'darkslateblue', 'darkslategray', 'darkturquoise',
                     'darkviolet', 'deeppink', 'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'floralwhite',
                     'forestgreen', 'fuchsia', 'gainsboro', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green',
                     'greenyellow', 'honeydew', 'hotpink', 'indianred', 'indigo', 'ivory', 'khaki', 'lavender',
                     'lavenderblush', 'lawngreen', 'lemonchiffon', 'lightblue', 'lightcoral', 'lightcyan',
                     'lightgoldenrodyellow', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen',
                     'lightskyblue', 'lightslategray', 'lightsteelblue', 'lightyellow', 'lime', 'limegreen', 'linen',
                     'magenta', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid', 'mediumpurple',
                     'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise', 'mediumvioletred',
                     'midnightblue', 'mintcream', 'mistyrose', 'moccasin', 'navajowhite', 'navy', 'oldlace', 'olive',
                     'olivedrab', 'orange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise',
                     'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink', 'plum', 'powderblue', 'purple', 'red',
                     'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen', 'seashell', 'sienna',
                     'silver', 'skyblue', 'slateblue', 'slategray', 'snow', 'springgreen', 'steelblue', 'tan', 'teal',
                     'thistle', 'tomato', 'turquoise', 'violet', 'wheat', 'white', 'whitesmoke', 'yellow',
                     'yellowgreen',
                     'aliceblue', 'antiquewhite', 'aqua', 'aquamarine', 'azure', 'beige', 'bisque', 'black',
                     'blanchedalmond', 'blue', 'blueviolet', 'brown', 'burlywood', 'cadetblue', 'chartreuse',
                     'chocolate',
                     'coral', 'cornflowerblue', 'cornsilk', 'crimson', 'cyan', 'darkblue', 'darkcyan', 'darkgoldenrod',
                     'darkgray', 'darkgreen', 'darkkhaki', 'darkmagenta', 'darkolivegreen', 'darkorange', 'darkorchid',
                     'darkred', 'darksalmon', 'darkseagreen', 'darkslateblue', 'darkslategray', 'darkturquoise',
                     'darkviolet', 'deeppink', 'deepskyblue', 'dimgray', 'dodgerblue', 'firebrick', 'floralwhite',
                     'forestgreen', 'fuchsia', 'gainsboro', 'ghostwhite', 'gold', 'goldenrod', 'gray', 'green',
                     'greenyellow', 'honeydew', 'hotpink', 'indianred', 'indigo', 'ivory', 'khaki', 'lavender',
                     'lavenderblush', 'lawngreen', 'lemonchiffon', 'lightblue', 'lightcoral', 'lightcyan',
                     'lightgoldenrodyellow', 'lightgreen', 'lightgrey', 'lightpink', 'lightsalmon', 'lightseagreen',
                     'lightskyblue', 'lightslategray', 'lightsteelblue', 'lightyellow', 'lime', 'limegreen', 'linen',
                     'magenta', 'maroon', 'mediumaquamarine', 'mediumblue', 'mediumorchid', 'mediumpurple',
                     'mediumseagreen', 'mediumslateblue', 'mediumspringgreen', 'mediumturquoise', 'mediumvioletred',
                     'midnightblue', 'mintcream', 'mistyrose', 'moccasin', 'navajowhite', 'navy', 'oldlace', 'olive',
                     'olivedrab', 'orange', 'orangered', 'orchid', 'palegoldenrod', 'palegreen', 'paleturquoise',
                     'palevioletred', 'papayawhip', 'peachpuff', 'peru', 'pink', 'plum', 'powderblue', 'purple', 'red',
                     'rosybrown', 'royalblue', 'saddlebrown', 'salmon', 'sandybrown', 'seagreen', 'seashell', 'sienna',
                     'silver', 'skyblue', 'slateblue', 'slategray', 'snow', 'springgreen', 'steelblue', 'tan', 'teal',
                     'thistle', 'tomato', 'turquoise', 'violet', 'wheat', 'white', 'whitesmoke', 'yellow',
                     'yellowgreen']

        pg_x = 1000
        pg_y = 1000
        pg_x_buff = 50
        pg_y_buff = 50
        px_buffer = 100
        py_buffer = 20

        win = GraphWin("Track Map - Click mouse in window to close", pg_x, pg_y)
        win.setBackground("white")
        nt = 0

        tlx = pg_x_buff
        tly = pg_y_buff
        brx = pg_x - pg_x_buff
        bry = pg_y - pg_y_buff

        for true_track in true_tracks:
            nt += 1

            s = true_track.track_pt_id()
            r = int(hashlib.sha256(s.encode('utf-8')).hexdigest(), 16) % 256

            tx = py_buffer
            ty = nt * py_buffer

            circ = Circle(Point(tx, ty), 4)
            circ.setFill(colorlist[r])
            circ.setOutline(colorlist[r])
            circ.draw(win)

            atxt = Text(Point(tx + px_buffer, ty), s)
            atxt.setFace("arial")
            atxt.setSize(8)
            atxt.setStyle("bold")
            atxt.setTextColor(colorlist[r])
            atxt.draw(win)

            for pt in true_track.pt_list:
                tx = interp(pt.lon_deg, x_min, x_max, tlx, brx)
                ty = tly + bry - interp(pt.lat_deg, y_min, y_max, tly, bry)
                circ = Circle(Point(tx, ty), 4)
                circ.setOutline(colorlist[r])
                circ.setFill(colorlist[r])
                circ.draw(win)

        for ntl_track in ntl_tracks:

            s = ntl_track.track_pt_id()
            r = int(hashlib.sha256(s.encode('utf-8')).hexdigest(), 16) % 256

            for i in range(1, len(ntl_track.pt_list)):
                tx1 = interp(ntl_track.pt_list[i].lon_deg, x_min, x_max, tlx, brx)
                ty1 = tly + bry - interp(ntl_track.pt_list[i].lat_deg, y_min, y_max, tly, bry)

                tx2 = interp(ntl_track.pt_list[i - 1].lon_deg, x_min, x_max, tlx, brx)
                ty2 = tly + bry - interp(ntl_track.pt_list[i - 1].lat_deg, y_min, y_max, tly, bry)

                aline = Line(Point(tx1, ty1), Point(tx2, ty2))
                aline.setWidth(2)

                # if ntl_track.pt_list[i].pt_id == ntl_track.track_id() and \
                #        ntl_track.pt_list[i].pt_id == ntl_track.pt_list[i - 1].pt_id and \
                #        ntl_track.pt_list[i].trk_idx == ntl_track.pt_list[i - 1].trk_idx + 1:
                #    aline.setOutline(colorlist[r])
                # else:
                aline.setOutline("black")

                aline.draw(win)

        win.getMouse()
        win.close()


# main code starts here


def main():
    if len(sys.argv) != 5 and len(sys.argv) != 6:
        print('usage: python TrackAssoc_AIS.py <data_filename (in quotes)> <alpha_v> <alpha_dh> <alpha_d2h> [max_v_ms]')
        exit(1)

    t_data_file = sys.argv[1]

    if not os.path.exists(t_data_file):
        print('Error: Data File {} Does Not Exist.'.format(t_data_file))
        exit(1)

    t_alpha_v = float(sys.argv[2])
    t_alpha_dh = float(sys.argv[3])
    t_alpha_d2h = float(sys.argv[4])
    t_max_v_ms = float(1000000000)

    if len(sys.argv) == 6:
        t_max_v_ms = float(sys.argv[5])

    track_assoc_AIS(t_data_file, t_alpha_v, t_alpha_dh, t_alpha_d2h, t_max_v_ms)

main()
