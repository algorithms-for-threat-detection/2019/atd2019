# ATD2019 AIS Challenge

The ATD2019 challenge considers the problem of reconstructing maritime tracks (sequences of posits associated with a particular vessel)
from a set of unlabelled, unsequenced posits.

The ATD2019 challenge is part of the Algorithms for Threat Detection program.
The challenge was adminstered by the RAND corporation.

This repository serves as a mirror for the challenge's publicly releasable content. Some documentation material has been omitted due to the presence of *Not for public release* markings.

